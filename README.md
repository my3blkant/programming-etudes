# Programming Etudes
Set of simple programs written in different languages.

[Etudes](#etudes)

[Requirements](#requirements)

[Future Plans](#future-plans)

## Etudes
* [Hello World](./hello_world/README.md) — prints "Hello, World!"

## Requirements
Examples are written and executed on Fedora 32. The code isn't intended to be cross-platform.
* Fedora 32
  

### C++
G++ compiler:
> sudo dnf install gcc-c++

### C#
Mono:
> sudo dnf install mono-core

DotNet:
> sudo dnf install dotnet dotnet-sdk-3.1 dotnet-runtime-3.1

### Deno
Using Rust's [Cargo](https://www.rust-lang.org/tools/install):
> cargo install deno

### Erlang
  
> sudo dnf install erlang-erts

### Go
Install [manually](https://golang.org/doc/install) or 
> sudo dnf install golang
  
### Groovy
With [SDKMAN!](https://sdkman.io/)
> curl -s "https://get.sdkman.io" | bash

> sdk install groovy

### Haskell
> sudo dnf install haskell-platform

### Java
> sudo dnf install java-11-openjdk

### Node.js
[Manually](https://nodejs.org/en/download/) or
> sudo dnf install nodejs

### PHP
> sudo dnf install php php-cli

### Python (v.3)
> sudo dnf install python3

### Ruby
> sudo dnf install ruby

### Rust
Install [Rust](https://www.rust-lang.org/tools/install) using rustup:
> curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

## Future plans

  * [ ] More Etudes
    * [ ] Prime Numbers
    * [ ] Math
    * [ ] CLI
    * [ ] HTTP Client, HTTP Server
    * [ ] NCurses
    * [ ] Serialization
    * [ ] Threading, multiprocessing, IPC
    * [ ] Streams demo
    * [ ] Archive Navigator
    * [ ] Redis interaction
    * [ ] DB interaction
      * [ ] sqlite
      * [ ] mysql
      * [ ] mongodb
    * [ ] GRPC
    * [ ] Tk
    * [ ] DBus
    * [ ] WAV - player
    * [ ] OpenGL
    * [ ] OpenCV
    * [ ] Writing extensions (if applicable)
    * [ ] Integrate Lua interpretator
  * [ ] Compile and run through Docker
  * [ ] Code styling, linting