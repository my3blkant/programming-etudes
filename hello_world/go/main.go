package main

func sayHello(name string) {
	println("Hello, " + name + "!")
}

func main() {
	sayHello("World")
}
