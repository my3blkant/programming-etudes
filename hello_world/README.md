# Hello World
[Back](../README.md)

### Task
    Print "Hello, World!" to stdout.

### Compile
> make

### Run
> make run

### Clean
> make clean


