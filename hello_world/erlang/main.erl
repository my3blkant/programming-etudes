-module(main).

-export([main/0]).

sayHello(Name) -> io:fwrite("Hello, " ++ Name ++ "!\n").

main() -> sayHello("World").
