sayHello :: String -> IO()
sayHello name = putStrLn $ "Hello, " ++ name ++ "!"

main :: IO()
main = do
  sayHello "World"