#!/usr/bin/env python3

def say_hello(name):
  print("Hello, {}!".format(name))

if __name__ == '__main__':
  say_hello("World")