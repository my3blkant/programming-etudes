#include <cstdio>

void sayHello(const char *name) {
  printf("Hello, %s!\n", name);
}

int main(int argc, char **argv)
{
  sayHello("World");
}
