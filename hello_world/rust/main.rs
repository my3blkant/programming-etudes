fn say_hello(name: &str) {
  println!("Hello, {}!", name);
}

fn main() {
  say_hello("World")
}