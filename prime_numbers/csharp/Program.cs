﻿using System;
using System.Collections.Generic;

namespace PrimeNumbers
{
  class Program
  {
    const int FIRST_PRIMES_QUANTITY = 100;

    static void Main(string[] args)
    {
      var primes = new Primes();

      primes
        .generate(FIRST_PRIMES_QUANTITY)
        .print();
    }
  }
}
