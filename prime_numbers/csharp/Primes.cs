using System;
using System.Collections.Generic;

namespace PrimeNumbers
{
  class Primes
  {
    private List<int> primes;
    public Primes generate(int count)
    {
      primes = new List<int>();

      primes.Add(2);

      int test_number = 3;
      bool is_prime;
      while (primes.Count < count)
      {
        is_prime = true;
        foreach (var prime in primes)
        {
          if (test_number % prime == 0)
          {
            is_prime = false;
            break;
          }
          if (test_number < prime << 1)
          {
            break;
          }
        }
        if (is_prime)
        {
          primes.Add(test_number);
        }
        test_number += 2;
      }
      return this;
    }

    public void print()
    {
      System.Console.Write("[");
      int index = 0;
      foreach (var prime in primes)
      {
        System.Console.Write(prime.ToString());
        if (index++ < primes.Count - 1)
        {
          System.Console.Write(", ");
        }
      }
      System.Console.WriteLine("]");
    }
  }
}