class Primes {
  count: number;
  primes: number[] = [];

  constructor(count: number) {
    this.count = count
    this.generate();
  }

  private generate() {
    this.primes.push(2);
    let test_number = 3;
    while (this.primes.length < this.count) {
      let is_prime = true;
      for (let prime of this.primes) {
        if (test_number % prime == 0) {
          is_prime = false;
          break;
        }
        if (prime << 1 > test_number) {
          break;
        }
      }

      if (is_prime) {
        this.primes.push(test_number);
      }

      test_number += 2;
    }
  }

  toArray(): number[] {
    return this.primes;
  }

}

export default Primes;