import Primes from './primes.ts';

const FIRST_PRIMES_QUANTITY = 100;

let primes = new Primes(100);

console.log("[%s]", primes.toArray().join(', '))
