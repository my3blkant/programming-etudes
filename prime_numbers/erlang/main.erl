-module(main).

-export([main/0]).

% Entry Point
main() ->
    First100Primes = take(100, primeNumbersSeq()),
    print(First100Primes),
    ok.

% Print result
print(PrimeNumbers) ->
    io:fwrite("["),
    printItems(PrimeNumbers),
    io:fwrite("]~n").
  

printItems([]) -> ok;
printItems([Last]) -> io:fwrite("~w",[Last]);
printItems([Next| Rest]) -> io:fwrite("~w, ",[Next]), printItems(Rest).


% Create sequence of prime numbers
primeNumbersSeq() ->
    FirstPrime = 2,
    TailFun = fun () -> primeNumbersNext(oddNumbersSeq(3))
	      end,
    [FirstPrime | TailFun].

primeNumbersNext(_Sequence = [Next | TailFun]) ->
    NewTailFun = fun () ->
			 NewSequence = filter(notDividableBy(Next), TailFun()),
			 primeNumbersNext(NewSequence)
		 end,
    [Next | NewTailFun].

% Odd Numbers Sequence
oddNumbersSeq(Start) ->
    TailFun = fun () -> oddNumbersSeq(Start + 2) end,
    [Start | TailFun].

% Take first 'Count' items from Sequence
take(0, _Sequence) -> [];
take(_Count, _Sequence = []) -> [];
take(Count, _Sequence = [Next | TailFun]) ->
    [Next | take(Count - 1, TailFun())].

% Predicate for prime numbers' filter
notDividableBy(D) -> fun (X) -> X rem D /= 0 end.

% Filter Sequence
filter(_Predicate, _Sequence = []) -> [];
filter(Predicate, _Sequence = [Next | TailFun]) ->
    case Predicate(Next) of
      true ->
	  NewTailFun = fun () -> filter(Predicate, TailFun()) end,
	  [Next | NewTailFun];
      false -> filter(Predicate, TailFun())
    end.
