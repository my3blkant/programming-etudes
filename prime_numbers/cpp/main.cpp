#include <iostream>
#include <vector>
#include "primes.hpp"

using namespace std;

const int FIRST_PRIMES_QUANTITY = 100;

void printPrimes(vector<int> &primes)
{
  int size = primes.size();
  int index = 0;
  cout << "[";
  for (int prime : primes)
  {
    cout << prime;
    if (index++ < size - 1)
    {
      cout << ", ";
    }
  }
  cout << "]" << endl;
}

int main(int argc, char **argv)
{
  auto primes = getFirstPrimes(FIRST_PRIMES_QUANTITY);

  printPrimes(primes);
}
