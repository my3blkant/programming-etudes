#include "primes.hpp"

const int FIRST_PRIME_NUMBER = 2;
const int SECOND_PRIME_NUMBER = 3;

vector<int> getFirstPrimes(int count)
{
  vector<int> primes;

  // First prime number: 2
  primes.push_back(FIRST_PRIME_NUMBER);

  // Start checking from 3
  int test_number = SECOND_PRIME_NUMBER;

  while (primes.size() < count)
  {
    bool is_prime = true;

    for (int prime : primes)
    {
      // If remainder is zero, not a prime number
      if (test_number % prime == 0)
      {
        is_prime = false;
        break;
      }
      // If prime number is greater than half of test_number, skip next prime numbers
      if (prime << 1 > test_number)
      {
        break;
      }
    }

    if (is_prime)
    {
      primes.push_back(test_number);
    }

    // We only interested in odd numbers: 3 + 2n, n = 0,1,...
    test_number += 2;
  }

  return primes;
}